import { useEffect, useState } from 'react'

export default function useApi(url, add) {
    const [state, setState] = useState({})

    async function apiReq() {
        try {
            setState({ ...state, loading: true })
            let res = await fetch(url)
            if (res.status >= 400) throw res.data || res
            res = await res.json(url)

            if (state.data && add)
                res = [...state.data, ...res]

            setState({ data: res })
        } catch (error) {
            setState({ error: error.message || error.data || 'error' })
        }
    }

    useEffect(() => {
        apiReq()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [url])

    return state
}