import Header from 'layout/Header'
import CommentsList from 'layout/CommentsList'
import AddComment from 'layout/AddComment'

function App() {
  return <div className='app'>
    <Header />
    <main>
      <AddComment />
      <CommentsList />
    </main>
  </div>
}

export default App
