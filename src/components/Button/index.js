import styles from './button.module.scss'

export default function Button({ className = '', children, ...props }) {
    return <button className={`${styles.button} ${className}`} {...props}>
        {children}
    </button>
}