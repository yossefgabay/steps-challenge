import styles from './errorText.module.scss'

export default function ErrorText({ className = '', error }) {
    return <div className={`${styles.errorText} ${className}`}>
        {error}
    </div>
}