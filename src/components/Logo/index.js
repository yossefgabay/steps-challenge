import styles from './logo.module.scss'
import logo from './logo.svg'

export default function Logo({ className = '' }) {
    return <div className={`${styles.logo} ${className}`}>
        <img src={logo} alt='logo' />
    </div>
}