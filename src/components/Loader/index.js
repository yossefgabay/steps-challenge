import style from './loader.module.scss'

const Loader = ({ className = '', size }) =>
    <div
        className={`${style.loader} ${className}`}
        style={size && { width: size, height: size, top: .5 * size }}
    />

export default Loader
