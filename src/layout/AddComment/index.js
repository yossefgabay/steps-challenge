import styles from './addComment.module.scss'

const URL = 'http://test.steps.me/test/testAssignComment'

export default function AddComment({ className = '' }) {

    async function handleSubmit(e) {
        e.preventDefault()
        const values = Object.values(e.target)
            .reduce((acc, input) =>
                !input.name ? acc : ({
                    ...acc, [input.name]: input.value
                }), {}
            )
        try {
            const res = await fetch(URL, {
                method: 'post',
                body: JSON.stringify(values),
                headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
            })
            console.log(res)
        } catch (error) {
            console.log(error)
        }
        e.target.reset()
    }

    return <form className={`${styles.addComment} ${className}`} onSubmit={handleSubmit}>
        <div>
            <input name='name' placeholder='your name' required />
            <input name='email' placeholder='your email' type='email' required />
        </div>
        <div>
            <textarea name='body' required placeholder='say something' />
            <input type='submit' value='Add Comment' />
        </div>
    </form>
}