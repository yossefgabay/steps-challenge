import styles from './commentsList.module.scss'
import Loader from 'components/Loader'
import useApi from 'hooks/useApi'
import Comment from 'layout/Comment'
import ErrorText from 'components/ErrorText'
import Button from 'components/Button'
import { useState } from 'react'

const
    PER_PAGE = 20,
    BASE_URL = `https://jsonplaceholder.typicode.com/comments?_limit=${PER_PAGE}`

export default function CommentsList({ className = '' }) {

    const
        [page, setPage] = useState(0),
        url = `${BASE_URL}&_start=${page * PER_PAGE}`,
        { data: list = [], loading, error } = useApi(url, true)


    return <div className={`${styles.commentsList} ${className}`}>
        <h2>Comments</h2>

        {error && <ErrorText eror={error} />}

        {list.map(comment => <Comment key={comment.id} comment={comment} />)}

        {loading ? <Loader /> :
            <Button onClick={() => setPage(page + 1)}>More...</Button>}
    </div>
}