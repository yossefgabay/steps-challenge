import styles from './comment.module.scss'

export default function Comment({ className = '', comment = {} }) {

    const { name, email, body } = comment

    return <div className={`${styles.comment} ${className}`}>
        <img src='https://www.shareicon.net/data/512x512/2017/01/06/868320_people_512x512.png' alt={`${name} profile`} />
        <div>
            <label className={styles.name}>{name}</label>
            <span className={styles.email}>{email}</span>
            <p>{body}</p>
        </div>
    </div>
}