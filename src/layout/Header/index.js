import Logo from 'components/Logo'
import styles from './header.module.scss'

export default function Header() {
    return <header className={`${styles.header}`}>
        <h1>Challenge</h1>
        <Logo />
    </header>
}